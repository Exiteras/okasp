﻿using GestContact.Abstractions;
using GestContact.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestContact.Controllers
{
    public class ContactController : Controller
    {
        public IContactService _service;

        public ContactController(IContactService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            List<Contact> contacts = _service.GetAll();
            return View(contacts);
        }
        [HttpPost]
        public IActionResult Index([FromForm]Contact cnt)
        {

            _service.AddContact(cnt);
            List<Contact> contacts = _service.GetAll();
            return View(contacts);
        }
        public RedirectToActionResult RemoveContact(int id)
        {
            _service.RemoveContact(id);
            return RedirectToAction("Index");
        }
        
        
    }
}
